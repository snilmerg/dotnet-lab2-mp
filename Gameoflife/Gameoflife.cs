﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Gameoflife
{
    class Gameoflife
    {

        [DllImport("kernel32.dll", ExactSpelling = true)]

        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;



        static void Main(string[] args)
        {
            int y = 50;
            int x = 50;
            int steps = 600;
            int time = 150;
            int random = x * y / 6;

            Scene Scena = new Scene(y,x);
            Console.SetWindowSize(y+2, x+2);
            ShowWindow(ThisConsole, MAXIMIZE);

            // uklad startowy to Gosper Gilder Gun
            Scena.Insertgosper(); 

            // uklad startowy to losowo ozywiona 1/6 komorek
            //Scena.Insertrandom(random); 

            for (int z = 0; z < steps; z++)
            {
                Console.Clear();
                Console.WriteLine("krok: " + z);
                Scena.Output();
                System.Threading.Thread.Sleep(time);
                Scena.Nextstate();
            }
        }
    }
}
