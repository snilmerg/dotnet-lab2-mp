﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameoflife
{
    class Scene
    {
        private int[,] tab;
        private int dimy, dimx;

        public Scene(int zm1, int zm2)
        {
            dimy = zm1+2;
            dimx = zm2+2;
            tab = new int[dimy, dimx];
            for (int y = 0; y < dimy; y++)
            {
                for (int x = 0; x < dimx; x++)
                {
                    tab[y, x] = 0;
                }
            }
        }

        public void Output()
        {
            for (int y = 1; y < dimy-1; y++)
            {
                for (int x = 1; x < dimx-1; x++)
                {   
                    if(tab[y, x]==0)
                        Console.Write(" ");
                    else
                        Console.Write("X");
                }
                Console.Write("\n");
            }
        }

        public void Insertgosper()
        {
            tab[5, 1] = 1;
            tab[5, 2] = 1;
            tab[6, 1] = 1;
            tab[6, 2] = 1;

            tab[5, 11] = 1;
            tab[6, 11] = 1;
            tab[7, 11] = 1;

            tab[4, 12] = 1;
            tab[8, 12] = 1;

            tab[3, 13] = 1;
            tab[3, 14] = 1;
            tab[9, 13] = 1;
            tab[9, 14] = 1;

            tab[6, 15] = 1;
            tab[4, 16] = 1;
            tab[8, 16] = 1;
            ////
            tab[5, 17] = 1;
            tab[6, 17] = 1;
            tab[7, 17] = 1;
            tab[6, 18] = 1;

            tab[3, 21] = 1;
            tab[4, 21] = 1;
            tab[5, 21] = 1;
            tab[3, 22] = 1;
            tab[4, 22] = 1;
            tab[5, 22] = 1;

            tab[2, 23] = 1;
            tab[6, 23] = 1;

            tab[1, 25] = 1;
            tab[2, 25] = 1;
            tab[6, 25] = 1;
            tab[7, 25] = 1;

            tab[3, 35] = 1;
            tab[4, 35] = 1;
            tab[3, 36] = 1;
            tab[4, 36] = 1;
        }

        public void Insertgilder(int a, int b) {
            tab[a, b+2] = 1;
            tab[a+1, b + 2] = 1;
            tab[a + 2, b + 2] = 1;
            tab[a + 2, b + 1] = 1;
            tab[a + 1, b] = 1;
        }

        public void Insertrandom(int a)
        {
            Random rnd = new Random();
            int x = 0, y = 0;
            for (int b=0; b<a; b++)
            {
                x = rnd.Next(1, dimx-1);
                y = rnd.Next(1, dimy-1);
                tab[y, x] = 1;
            }
        }

        public void Nextstate() {
            int[,] next = new int[dimy, dimx];
            int sum = 0;
            // inserting zeros in next tab
            for (int y = 0; y < dimy; y++)
            {
                for (int x = 0; x < dimx; x++)
                {
                    next[y, x] = 0;
                }
            }
            // checking neighboors at every tab cell
            for (int y = 1; y < dimy - 1; y++)
            {
                for (int x = 1; x < dimx - 1; x++)
                {
                    sum = tab[y-1, x-1] + tab[y-1, x] + tab[y-1, x+1] + tab[y, x+1] + tab[y+1, x+1] + tab[y+1, x] + tab[y+1,x-1] + tab[y, x-1];
                    if (sum < 2 && tab[y, x] == 1)
                        next[y, x]=0;
                    else if((sum == 2 || sum==3) && tab[y, x] == 1)
                        next[y, x] = 1;
                    else if (sum >3 && tab[y, x] == 1)
                        next[y, x] = 0;
                    else if (sum == 3 && tab[y, x] == 0)
                        next[y, x] = 1;

                    sum = 0;
                }
            }
            // inserting cells in tab
            for (int y = 0; y < dimy; y++)
            {
                for (int x = 0; x < dimx; x++)
                {
                    tab[y, x] = next[y, x];
                }
            }
        }


    }
}
